import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { MarkdownToHtmlModule } from 'ng2-markdown-to-html';
import { MaterializeModule } from 'ng2-materialize';

import { ROUTES } from './app.routing';

import { AppComponent } from './app.component';
import { ModalModule } from './modal/modal.module';

@NgModule({
  imports: [
    // external modules
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    MarkdownToHtmlModule.forRoot(),
    MaterializeModule.forRoot(),
    ReactiveFormsModule,
    RouterModule.forRoot(ROUTES),

    // internal modules
    ModalModule
  ],
  declarations: [AppComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
