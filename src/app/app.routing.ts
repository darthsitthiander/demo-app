// tslint:disable:max-line-length

import { Routes } from '@angular/router';

import { ModalComponent } from './modal/modal.component';

// sections name
const css = 'CSS';
const formControls = 'Form Controls';
const layout = 'Layout';
const loading = 'Loading';
const indicator = 'Indicators';
const service = 'Services';

export const ROUTES: Routes = [
  // home route
  { path: '', component: ModalComponent, data: { icon: 'image-filter-none', text: 'Modal', section: layout } },

  // components routes - Layout
  { path: 'modal', component: ModalComponent, data: { icon: 'image-filter-none', text: 'Modal', section: layout } },

  // redirect to home when route does not exists (must be last route)
  { path: '**', redirectTo: 'home' },
];
