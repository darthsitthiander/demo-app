import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MaterializeModule } from 'ng2-materialize';

import { ModalExampleComponent } from './modal-example/modal-example.component';
import { ModalComponent } from './modal.component';

@NgModule({
  imports: [
    CommonModule,
    MaterializeModule.forRoot(),
    RouterModule,
  ],
  declarations: [
    ModalComponent,
    ModalExampleComponent,
  ],
  entryComponents: [
    ModalExampleComponent,
  ],
})
export class ModalModule { }
