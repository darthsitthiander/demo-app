import { Component, OnInit } from '@angular/core';
import { MzModalService } from 'ng2-materialize';

import { ModalExampleComponent } from './modal-example/modal-example.component';
import { MzInjectionService } from 'ng2-materialize/dist/shared/injection/injection.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  constructor(
    private modalService: MzModalService,
    private injectionService: MzInjectionService
  ) { }

  ngOnInit() {
    this.injectionService.setRootViewContainer({
      hostView: {
        rootNodes: [document.body],
      },
    });
  }

  openServiceModal() {
    this.modalService.open(ModalExampleComponent);
  }
}
